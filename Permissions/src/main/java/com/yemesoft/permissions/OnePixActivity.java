package com.yemesoft.permissions;

import android.app.Activity;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.view.ViewGroup;
import android.view.Window;
import android.view.WindowManager;
import android.widget.FrameLayout;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.annotation.Nullable;

public class OnePixActivity extends Activity {
    public static final String EXTRA_DESCRIPTIONS = "EXTRA_DESCRIPTIONS";

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));

        if (null != getActionBar()) {
            getActionBar().hide();
        }

//        WindowManager.LayoutParams lp = getWindow().getAttributes();
//        lp.width = 1;
//        lp.height = 1;
//        lp.x = -1;
//        lp.y = -1;
//        getWindow().setAttributes(lp);
    }
}
