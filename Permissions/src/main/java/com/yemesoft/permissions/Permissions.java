package com.yemesoft.permissions;

import android.Manifest;
import android.content.Context;
import android.content.Intent;

import com.yemesoft.permissions.handlers.common.PermissionsHandler;
import com.yemesoft.permissions.handlers.floatwindow.FloatHandler;
import com.yemesoft.permissions.handlers.storage.StorageHandler;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

public class Permissions {
    public static final String SYSTEM_OVERLAY_WINDOW = "android.permission.SYSTEM_OVERLAY_WINDOW";
    private static final Map<String, Class<? extends PermissionRequestHandler>> handlerMap = new HashMap<>();

    static {
        handlerMap.put(Manifest.permission.WRITE_EXTERNAL_STORAGE, StorageHandler.class);
        handlerMap.put(Manifest.permission.READ_EXTERNAL_STORAGE, StorageHandler.class);
        handlerMap.put(Manifest.permission.MANAGE_EXTERNAL_STORAGE, StorageHandler.class);
        handlerMap.put(Manifest.permission.READ_MEDIA_AUDIO, StorageHandler.class);
        handlerMap.put(Manifest.permission.READ_MEDIA_VIDEO, StorageHandler.class);
        handlerMap.put(Manifest.permission.READ_MEDIA_IMAGES, StorageHandler.class);

        handlerMap.put(Manifest.permission.SYSTEM_ALERT_WINDOW, FloatHandler.class);
        handlerMap.put(Permissions.SYSTEM_OVERLAY_WINDOW, FloatHandler.class);

        handlerMap.put(Manifest.permission.POST_NOTIFICATIONS, FloatHandler.class);
    }

    private static List<PermissionRequestHandler> getHandlers(Context context, String... permissions) {
        try {
            Map<Class<? extends PermissionRequestHandler>, PermissionRequestHandler> map = new LinkedHashMap<>();
            for (String permission : permissions) {
                Class<? extends PermissionRequestHandler> handlerClass = handlerMap.get(permission);
                if (null == handlerClass) {
                    handlerClass = PermissionsHandler.class;
                }
                PermissionRequestHandler handler = map.get(handlerClass);
                if (null == handler) {
                    handler = handlerClass.newInstance();
                    map.put(handlerClass, handler);
                }
                handler.addPermission(context, permission);
            }
            return new ArrayList<>(map.values());
        } catch (Exception e) {
            e.printStackTrace();
        }
        return new ArrayList<>();
    }

    public static void request(Context context, PermissionCallback callback, String... permissions) {
        request(context, null, callback, permissions);
    }

    public static void request(Context context, String[] descriptions, PermissionCallback callback, String... permissions) {
        List<PermissionRequestHandler> handlers = getHandlers(context, permissions);
        if (0 == handlers.size()) {
            callback.onComplete(new ArrayList<String>(), new ArrayList<String>());
            return;
        }
        for (int i = handlers.size() - 1; i >= 0; i--) {
            PermissionRequestHandler handler = handlers.get(i);
            if (handler.hasAllPermissions(context)) {
                handlers.remove(i);
            }
        }
        boolean hasAllPermissions = true;

        if (null != descriptions) {
            DescriptionActivity.start(context, descriptions, callback, permissions);
        } else {
            processHandler(handlers, 0, context, new ArrayList<String>(), new ArrayList<String>(), callback);
        }
    }

    private static void processHandler(final List<PermissionRequestHandler> handlers, final int index, final Context context, final List<String> granted, final List<String> rejected, final PermissionCallback callback) {
        if (index >= handlers.size()) {
            callback.onComplete(granted, rejected);
            return;
        }
        handlers.get(index).handle(context, new PermissionCallback() {
            @Override
            public void onComplete(List<String> g, List<String> r) {
                granted.addAll(g);
                rejected.addAll(r);
                processHandler(handlers, index + 1, context, granted, rejected, callback);
            }
        });
    }
}
